To install this plugin follow these steps:

Login to your WordPress site
Select Plugins > Add New
Press Upload Plugin and choose the zip file.
Activate plugin.
Configuring Freight Center

Go to WooCommerce > Settings, and press the Shipping tab
Select Freight Center
Enable Freight Center
Set the mode to Sandbox for testing, and Live to receive live quotes
Add the Username and Password supplied by Freight Center
Press Save Changes
Requires: 3.0.1 or higher
Compatible up to: 4.4.5
